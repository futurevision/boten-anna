<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SlackRequestFactory
{
    const VERSION_NUMBER = 'v0';

    /**
     * @var string
     */
    private $key;

    /**
     * @var bool
     */
    private $verify;

    /**
     * @param string $key
     * @param bool $verify
     */
    public function __construct(string $key, bool $verify = true)
    {
        $this->key = $key;
        $this->verify = $verify;
    }

    /**
     * @param Request $request
     * @return SlackRequest
     */
    public function fromRequest(Request $request)
    {
        if ($this->verify) {
            $this->assertSignature($request);
        }

        return new SlackRequest($request);
    }

    /**
     * Verifies if this request really is signed by Slack
     * https://api.slack.com/docs/verifying-requests-from-slack
     *
     * @param Request $request
     */
    private function assertSignature(Request $request)
    {
        $signatureData = $this->getSignatureData($request);
        if (!hash_equals($this->sign($signatureData), $request->headers->get(SlackRequest::REQUEST_SIGNATURE, ''))) {
            throw new AccessDeniedHttpException('Invalid signature');
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    private function getSignatureData(Request $request): string
    {
        return sprintf(
            '%s:%s:%s',
            self::VERSION_NUMBER,
            $request->headers->get(SlackRequest::REQUEST_TIME_HEADER),
            $request->getContent()
        );
    }

    /**
     * @param string $data
     * @return string
     */
    private function sign(string $data): string
    {
        return sprintf(
            '%s=%s',
            self::VERSION_NUMBER,
            hash_hmac('sha256', $data, $this->key)
        );
    }
}
