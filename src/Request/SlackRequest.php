<?php

namespace App\Request;

use App\Request\SlackRequest\ChannelId;
use App\Request\SlackRequest\ChannelName;
use App\Request\SlackRequest\Command;
use App\Request\SlackRequest\ResponseUrl;
use App\Request\SlackRequest\TeamDomain;
use App\Request\SlackRequest\TeamId;
use App\Request\SlackRequest\Text;
use App\Request\SlackRequest\TriggerId;
use App\Request\SlackRequest\UserId;
use App\Request\SlackRequest\UserName;
use Symfony\Component\HttpFoundation\Request;

class SlackRequest
{
    const REQUEST_TIME_HEADER = 'x-slack-request-timestamp';
    const REQUEST_SIGNATURE = 'x-slack-signature';

    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return TeamId
     */
    public function getTeamId(): TeamId
    {
        return new TeamId($this->request->request->get('team_id', ''));
    }

    /**
     * @return TeamDomain
     */
    public function getTeamDomain(): TeamDomain
    {
        return new TeamDomain($this->request->request->get('team_domain', ''));
    }

    /**
     * @return ChannelId
     */
    public function getChannelId(): ChannelId
    {
        return new ChannelId($this->request->request->get('channel_id', ''));
    }

    /**
     * @return ChannelName
     */
    public function getChannelName(): ChannelName
    {
        return new ChannelName($this->request->request->get('channel_name', ''));
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return new UserId($this->request->request->get('user_id', ''));
    }

    /**
     * @deprecated Slack is phasing out user names, better not to use this
     * @return UserName
     */
    public function getUserName(): UserName
    {
        return new UserName($this->request->request->get('user_name', ''));
    }

    /**
     * @return Text
     */
    public function getText(): Text
    {
        return new Text($this->request->request->get('text', ''));
    }

    /**
     * @return Command
     */
    public function getCommand(): Command
    {
        return new Command($this->request->request->get('command', ''));
    }

    /**
     * @return ResponseUrl
     */
    public function getResponseUrl(): ResponseUrl
    {
        return new ResponseUrl($this->request->request->get('response_url', ''));
    }

    /**
     * @return TriggerId
     */
    public function getTriggerId(): TriggerId
    {
        return new TriggerId($this->request->request->get('trigger_id', ''));
    }

    /**
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getCreated(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('@'.$this->request->headers->get(self::REQUEST_TIME_HEADER, time()));
    }
}
