<?php

namespace App\Request\SlackRequest;

class TeamDomain extends AbstractText
{
    /**
     * @return bool
     */
    public function isMediaMonks(): bool
    {
        return $this->is('mediamonks');
    }
}
