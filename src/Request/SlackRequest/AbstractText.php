<?php

namespace App\Request\SlackRequest;

abstract class AbstractText
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->value);
    }

    /**
     * @param string $needle
     * @return bool
     */
    public function startsWith(string $needle): bool
    {
        return strpos($this->value, $needle) === 0;
    }

    /**
     * @param string $needle
     * @return bool
     */
    public function contains(string $needle): bool
    {
        return strpos($this->value, $needle) !== false;
    }

    /**
     * @param string $needle
     * @return bool
     */
    public function is(string $needle): bool
    {
        return $this->value === $needle;
    }

    /**
     * @param string $pattern
     * @return bool
     */
    public function matches(string $pattern): bool
    {
        return (bool)preg_match($pattern, $this->value);
    }
}
