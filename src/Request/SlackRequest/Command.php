<?php

namespace App\Request\SlackRequest;

class Command extends AbstractText
{
    public function __construct($value)
    {
        parent::__construct(ltrim($value, '/'));
    }
}
