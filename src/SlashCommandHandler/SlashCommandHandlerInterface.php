<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackResponse;

interface SlashCommandHandlerInterface
{
    public function supports(SlackRequest $request): bool;

    public function handle(SlackRequest $request): SlackResponse;
}
