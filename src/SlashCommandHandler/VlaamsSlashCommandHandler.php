<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackInChannelResponse;
use App\Response\SlackResponse;
use App\Service\VlaamsWoordenboek;

class VlaamsSlashCommandHandler implements SlashCommandHandlerInterface
{
    /**
     * @var VlaamsWoordenboek
     */
    private $vlaamsWoordenboek;

    /**
     * @param VlaamsWoordenboek $vlaamsWoordenboek
     */
    public function __construct(VlaamsWoordenboek $vlaamsWoordenboek)
    {
        $this->vlaamsWoordenboek = $vlaamsWoordenboek;
    }

    /**
     * @param SlackRequest $request
     * @return bool
     */
    public function supports(SlackRequest $request): bool
    {
        return $request->getCommand()->matches('~^(louise|vlaams)~');
    }

    /**
     * @param SlackRequest $request
     * @return SlackResponse
     */
    public function handle(SlackRequest $request): SlackResponse
    {
        $start = $request->getText()->getValue();
        if (empty($start) || !ctype_alpha($start)) {
            $start = range('a', 'z')[mt_rand(0, 25)];
        }

        $definition = $this->vlaamsWoordenboek->getRandomStartingWith($start);

        return new SlackInChannelResponse(
            sprintf('*%s* - _%s_', $definition->getWord(), $definition->getDescription())
        );
    }


}
