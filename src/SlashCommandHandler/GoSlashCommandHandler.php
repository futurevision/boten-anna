<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackEphemeralResponse;
use App\Response\SlackInChannelResponse;
use App\Response\SlackResponse;
use App\Service\MediaMonksGo;

class GoSlashCommandHandler implements SlashCommandHandlerInterface
{
    /**
     * @var MediaMonksGo
     */
    private $go;

    /**
     * @param MediaMonksGo $go
     */
    public function __construct(MediaMonksGo $go)
    {
        $this->go = $go;
    }

    /**
     * @param SlackRequest $request
     * @return bool
     */
    public function supports(SlackRequest $request): bool
    {
        return $request->getCommand()->is('go');
    }

    /**
     * @param SlackRequest $request
     * @return SlackResponse
     * @throws \Http\Client\Exception
     */
    public function handle(SlackRequest $request): SlackResponse
    {
        try {
            $url = $this->go->getUrl($request->getText()->getValue());
        }
        catch (\Exception $e) {
            $url = null;
        }
        if (empty($url)) {
            return new SlackEphemeralResponse('Url not found');
        }

        return new SlackInChannelResponse($url);
    }
}
