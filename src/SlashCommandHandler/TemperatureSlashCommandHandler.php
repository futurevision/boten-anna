<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackInChannelResponse;
use App\Response\SlackResponse;
use App\Service\Weather\WeatherInterface;

class TemperatureSlashCommandHandler implements SlashCommandHandlerInterface
{
    /**
     * @var WeatherInterface
     */
    private $weather;

    /**
     * @param WeatherInterface $weather
     */
    public function __construct(WeatherInterface $weather)
    {
        $this->weather = $weather;
    }

    /**
     * @param SlackRequest $request
     * @return bool
     */
    public function supports(SlackRequest $request): bool
    {
        return $request->getCommand()->startsWith('temp');
    }

    /**
     * @param SlackRequest $request
     * @return SlackResponse
     */
    public function handle(SlackRequest $request): SlackResponse
    {
        return new SlackInChannelResponse(
            sprintf('It\'s currently %s°C in Hilversum', round($this->weather->getTemperature(), 2))
        );
    }
}
