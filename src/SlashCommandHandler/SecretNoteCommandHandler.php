<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\EmptySlackResponse;
use App\Response\SlackEphemeralResponse;
use App\Response\SlackInChannelResponse;
use App\Response\SlackResponse;
use App\Service\SecretNote;
use App\Service\Slack;

class SecretNoteCommandHandler implements SlashCommandHandlerInterface
{
    /**
     * @var SecretNote
     */
    private $secretNote;

    /**
     * @var Slack
     */
    private $slack;

    /**
     * @param SecretNote $secretNote
     * @param Slack $slack
     */
    public function __construct(SecretNote $secretNote, Slack $slack)
    {
        $this->secretNote = $secretNote;
        $this->slack = $slack;
    }

    /**
     * @param SlackRequest $request
     * @return bool
     */
    public function supports(SlackRequest $request): bool
    {
        return $request->getCommand()->startsWith('secretnote');
    }

    /**
     * @param SlackRequest $request
     *
     * @return SlackResponse
     * @throws \Exception
     */
    public function handle(SlackRequest $request): SlackResponse
    {
        if (!$request->getChannelName()->is('directmessage')) {
            return new SlackEphemeralResponse('You can only use this feature in direct messages');
        }

        try {
            $response = $this->secretNote->createNote($request->getText()->getValue());
            $text = sprintf('<@%s> sends you this secret note: %s', $request->getUserId()->getValue(), $response->getUrl());
            $this->slack->respondByWebhook($request, new SlackInChannelResponse($text));

            return new EmptySlackResponse();
        }
        catch (\Exception $e) {
            return new SlackEphemeralResponse($e->getMessage().':'.$e->getTraceAsString());
        }
    }
}
