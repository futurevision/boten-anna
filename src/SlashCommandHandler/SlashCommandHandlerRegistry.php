<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackResponse;

class SlashCommandHandlerRegistry
{
    /**
     * @var SlashCommandHandlerInterface[]
     */
    private $handlers;

    /**
     * @param iterable $handlers
     */
    public function __construct(iterable $handlers)
    {
        $this->handlers = $handlers;
    }

    /**
     * @param SlackRequest $slackRequest
     * @return SlackResponse|null
     */
    public function handle(SlackRequest $slackRequest): ?SlackResponse
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($slackRequest)) {
                return $handler->handle($slackRequest);
            }
        }

        return null;
    }
}
