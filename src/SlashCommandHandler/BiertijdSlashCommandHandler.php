<?php

namespace App\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Response\SlackInChannelResponse;
use App\Response\SlackResponse;
use App\Service\Biertijd;
use App\Request\SlackRequest\UserId;

class BiertijdSlashCommandHandler implements SlashCommandHandlerInterface
{
    /**
     * @var Biertijd
     */
    private $biertijd;

    /**
     * @param Biertijd $biertijd
     */
    public function __construct(Biertijd $biertijd)
    {
        $this->biertijd = $biertijd;
    }

    /**
     * @param SlackRequest $request
     * @return bool
     */
    public function supports(SlackRequest $request): bool
    {
        return $request->getCommand()->startsWith('beer')
            || $request->getCommand()->startsWith('bier')
            || $request->getCommand()->startsWith('wine')
            || $request->getCommand()->startsWith('tequila');
    }

    /**
     * @param SlackRequest $request
     * @return SlackResponse
     * @throws \Http\Client\Exception
     */
    public function handle(SlackRequest $request): SlackResponse
    {
        if ($request->getUserId()->is(UserId::USER_ID_RICHARD_NIEUWENHUIS)) {
            return new SlackInChannelResponse('Who is Richard?');
        }

        $timeZone = new \DateTimeZone('Europe/Amsterdam');
        $statusResponse = $this->biertijd->getStatus($timeZone);

        if ($statusResponse->getStatus()) {
            return new SlackInChannelResponse('YESSSSSSS!!!!11111111');
        }

        $now = new \DateTime('now', $timeZone);
        $diff = $statusResponse->getNext()->diff($now);

        $drink = 'beer';
        if ($request->getCommand()->startsWith('wine')) {
            $drink = 'wine';
        }
        if ($request->getCommand()->startsWith('tequila')) {
            $drink = 'tequila';
        }

        $message = sprintf('Nope, next %s time is in %s', $drink, $diff->format($this->getFormat($diff)));

        return new SlackInChannelResponse($message);
    }

    /**
     * @param \DateInterval $dateInterval
     * @return string
     */
    private function getFormat(\DateInterval $dateInterval): string
    {
        $segments = [];

        // display days if it's more than 1 day
        if ($dateInterval->days > 0) {
            $segments[] = '%d days';
        }
        // display hours if it's more than 1 hour OR 1 day
        if ($dateInterval->days > 0 || $dateInterval->h > 0) {
            if ($dateInterval->h === 1) {
                $segments[] = '%h hour';
            }
            else {
                $segments[] = '%h hours';
            }
        }
        // display minutes if it's more than 1 minute OR 1 hour OR 1 day
        if ($dateInterval->days > 0 || $dateInterval->h > 0 || $dateInterval->i > 0) {
            if ($dateInterval->i === 1) {
                $segments[] = '%i minute';
            }
            else {
                $segments[] = '%i minutes';
            }
        }
        // only display seconds if we're in the last minute
        if ($dateInterval->days === 0 && $dateInterval->h === 0 && $dateInterval->s !== 0) {
            if ($dateInterval->s === 1) {
                $segments[] = '%s second';
            }
            else {
                $segments[] = '%s seconds';
            }
        }

        return implode(', ', $segments);
    }
}
