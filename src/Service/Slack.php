<?php

namespace App\Service;

use App\Request\SlackRequest;
use App\Request\SlackRequest\ChannelId;
use App\Response\SlackResponse;
use GuzzleHttp\Client;

class Slack
{
    const URL_BASE = 'https://slack.com/api/';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
        $this->client = new Client([
            'base_uri' => self::URL_BASE
        ]);
    }

    /**
     * @param SlackRequest $request
     * @param SlackResponse $response
     */
    public function respondByWebhook(SlackRequest $request, SlackResponse $response)
    {
        $this->client->post($request->getResponseUrl()->getValue(), [
            'json' => $response->toArray()
        ]);
    }

    /**
     * @param ChannelId $channel
     * @param SlackResponse $response
     */
    public function postInChannel(ChannelId $channel, SlackResponse $response)
    {
         $this->client->post(self::URL_BASE.'chat.postMessage', [
            'headers' => [
                'Authorization' => sprintf('Bearer %s', $this->token)
            ],
            'json' => [
                'channel' => $channel->getValue(),
                'text' => $response->getText()
            ]
        ]);
    }
}
