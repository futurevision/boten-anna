<?php

namespace App\Service\SecretNote;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class SecretNoteResponse
{
    /**
     * @var array
     */
    private $data;


    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param ResponseInterface $response
     *
     * @return static
     * @throws \Exception
     */
    public static function fromResponse(ResponseInterface $response): self
    {
        if ($response->getStatusCode() !== Response::HTTP_CREATED) {
            throw new \Exception('Invalid SecretNote response:'.$response->getBody()->getContents());
        }

        $data = json_decode($response->getBody()->getContents(), true);

        return new self($data['data']);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->data['url'];
    }

    /**
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getExpiresAt(): \DateTimeImmutable
    {
        return new \DateTimeImmutable($this->data['expires_at']);
    }

    /**
     * @return string
     */
    public function getExpiresAfter(): string
    {
        return $this->data['expires_after'];
    }
}
