<?php

namespace App\Service\Weather;

class ApixuWeatherProvider extends AbstractWeatherProvider
{
    const URL = 'http://api.apixu.com/v1/current.json?key=85803c08446e4199b9b124107171605&q=Hilversum';

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        $data = $this->getDataByUrl(self::URL);

        return (float)$data['current']['temp_c'];
    }

}
