<?php

namespace App\Service\Weather;

class OpenWeatherMapWeatherProvider extends AbstractWeatherProvider
{
    const URL = 'http://api.openweathermap.org/data/2.5/weather?q=Utrecht,NL&appid=46830204492434c10d37a75557191770&units=metric';

    public function getTemperature(): float
    {
        $data = $this->getDataByUrl(self::URL);

        return (float)$data['main']['temp'];
    }
}
