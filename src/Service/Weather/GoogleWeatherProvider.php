<?php

namespace App\Service\Weather;

class GoogleWeatherProvider extends AbstractWeatherProvider
{
    const URL = 'https://www.google.nl/search?q=weather+hilversum';

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        $response = $this->client->get(self::URL);

        $html = $response->getBody()->getContents();

        $after = @explode('<span class="wob_t" style="display:inline">', $html)[1];
        if (empty($after)) {
            $after = @explode('<span class="wob_t" id="wob_tm" style="display:inline">', $html)[1];
        }

        return (float)substr($after, 0, 2);
    }
}
