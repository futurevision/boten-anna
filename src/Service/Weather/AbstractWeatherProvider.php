<?php

namespace App\Service\Weather;

use Http\Client\Common\HttpMethodsClient as Client;

abstract class AbstractWeatherProvider implements WeatherProviderInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return float
     */
    abstract public function getTemperature(): float;

    /**
     * @param string $url
     * @return array
     */
    protected function getDataByUrl(string $url): array
    {
        $response = $this->client->get($url);

        return json_decode($response->getBody()->getContents(), true);
    }
}
