<?php

namespace App\Service\Weather;

interface WeatherInterface
{
    public function getTemperature(): float;
}
