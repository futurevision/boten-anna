<?php

namespace App\Service\Weather;

class HighestTemperatureWeather implements WeatherInterface
{
    /**
     * @var WeatherInterface[]
     */
    private $providers = [];

    /**
     * @param iterable $providers
     */
    public function __construct(iterable $providers)
    {
        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
    }

    /**
     * @param WeatherInterface $provider
     */
    public function addProvider(WeatherInterface $provider)
    {
        $this->providers[] = $provider;
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        $highest = -100;
        foreach ($this->providers as $provider) {
            $temperature = $provider->getTemperature();
            if ($temperature > $highest) {
                $highest = $temperature;
            }
        }

        return $highest;
    }

}
