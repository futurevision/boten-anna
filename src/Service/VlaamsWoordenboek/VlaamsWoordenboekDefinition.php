<?php

namespace App\Service\VlaamsWoordenboek;

class VlaamsWoordenboekDefinition
{
    /**
     * @var string
     */
    private $word;

    /**
     * @var string
     */
    private $description;

    /**
     * @param string $word
     * @param string $description
     */
    public function __construct(string $word, string $description)
    {
        $this->word = $word;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
