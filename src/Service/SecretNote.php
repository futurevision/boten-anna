<?php

namespace App\Service;

use App\Service\SecretNote\SecretNoteResponse;
use GuzzleHttp\Client;

class SecretNote
{
    const URL_BASE = 'https://secretnote.mediamonks.net/api/';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param string $note
     *
     * @return SecretNoteResponse
     * @throws \Exception
     */
    public function createNote(string $note): SecretNoteResponse
    {
        $response = $this->client->post(self::URL_BASE.'notes', [
            'form_params' => [
                'note' => $note
            ]
        ]);

        return SecretNoteResponse::fromResponse($response);
    }
}
