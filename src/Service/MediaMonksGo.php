<?php

namespace App\Service;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class MediaMonksGo
{
    const URL_BASE = 'https://go.mediamonks.net/api/';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => self::URL_BASE
        ]);
    }

    /**
     * @param string $path
     *
     * @return string|null
     */
    public function getUrl(string $path): ?string
    {
        $response = $this->client->get('urls/'.$path);
        if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
            return null;
        }
        $data = json_decode($response->getBody()->getContents(), true);

        return $data['data']['url'];
    }
}
