<?php

namespace App\Service;

use App\Service\VlaamsWoordenboek\VlaamsWoordenboekDefinition;
use Goutte\Client;

class VlaamsWoordenboek
{
    const URL_BASE = 'http://www.vlaamswoordenboek.be';

    /**
     * @var Client
     */
    private $goutte;

    /**
     * @param Client $goutte
     */
    public function __construct(Client $goutte)
    {
        $this->goutte = $goutte;
    }

    /**
     * @param string $start
     * @return VlaamsWoordenboekDefinition
     */
    public function getRandomStartingWith(string $start): VlaamsWoordenboekDefinition
    {
        $crawler = $this->goutte->request('GET', sprintf(self::URL_BASE.'/definities/begintmet/%s', $start));

        $nodes = $crawler->filter('table td a[href^=\/definities]');
        $path = $nodes->getNode(mt_rand(0, $nodes->count() - 1))->getAttribute('href');

        return $this->getByPath($path);
    }

    /**
     * @param string $path
     * @return VlaamsWoordenboekDefinition
     */
    public function getByPath(string $path): VlaamsWoordenboekDefinition
    {
        $crawler = $this->goutte->request('GET', self::URL_BASE.$path);

        $word = $this->cleanValue($crawler->filter('div#centercontent > div.formfield > h2')->text());
        $description = $this->cleanValue($crawler->filter('div#centercontent > div.formfield > p')->text());

        //$sentence = $crawler->filter('#centercontent > div.formfield > h2 > a');

        return new VlaamsWoordenboekDefinition($word, $description);
    }

    /**
     * @param string $value
     * @return string
     */
    private function cleanValue(string $value): string
    {
        return trim(
        // replace multiple spaces with a single one
            preg_replace(
                '!\s+!',
                ' ',
                str_replace(
                    ["\n", "\r", "\t"],
                    ' ',
                    strip_tags($value)
                )
            )
        );
    }
}
