<?php

namespace App\Service;

use App\Service\Biertijd\BiertijdStatus;
use Http\Client\Common\HttpMethodsClient as Client;

class Biertijd
{
    const URL_BASE = 'https://ishetalbiertijd.nl/api/';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $user = 'mediamonks';

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user)
    {
        $this->user = $user;
    }

    /**
     * @param \DateTimeZone $dateTimeZone
     * @return BiertijdStatus
     * @throws \Http\Client\Exception
     */
    public function getStatus(\DateTimeZone $dateTimeZone): BiertijdStatus
    {
        $response = $this->client->get($this->getUrl('status?timeZone='.$dateTimeZone->getName()));

        return BiertijdStatus::fromResponse($response, $dateTimeZone);
    }

    /**
     * @param string $path
     * @return string
     */
    private function getUrl(string $path): string
    {
        return sprintf('%s%s/%s', self::URL_BASE, $this->user, $path);
    }
}
