<?php

namespace App\Service\Biertijd;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class BiertijdStatus
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * @param array $data
     * @param \DateTimeZone $timeZone
     */
    public function __construct(array $data, \DateTimeZone $timeZone)
    {
        // @todo verify data exists in array
        $this->data = $data;
        $this->timeZone = $timeZone;
    }

    /**
     * @param ResponseInterface $response
     * @param \DateTimeZone $timeZone
     * @return BiertijdStatus
     * @throws \Exception
     */
    public static function fromResponse(ResponseInterface $response, \DateTimeZone $timeZone)
    {
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new \Exception('Invalid biertijd response');
        }

        $data = json_decode($response->getBody()->getContents(), true);

        return new self($data['data'], $timeZone);
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->data['status'];
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getNext(): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $this->data['next'], $this->timeZone);
    }
}
