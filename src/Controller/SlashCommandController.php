<?php

namespace App\Controller;

use App\Request\SlackRequestFactory;
use App\Response\SlackEphemeralResponse;
use App\Response\SlackResponse;
use App\SlashCommandHandler\SlashCommandHandlerRegistry;
use Symfony\Component\HttpFoundation\Request;

class SlashCommandController
{
    /**
     * @param Request $request
     * @param SlackRequestFactory $slackRequestFactory
     * @param SlashCommandHandlerRegistry $slashHandlerRegistry
     * @return SlackResponse
     */
    public function __invoke(
        Request $request,
        SlackRequestFactory $slackRequestFactory,
        SlashCommandHandlerRegistry $slashHandlerRegistry
    ): SlackResponse {

        $slackRequest = $slackRequestFactory->fromRequest($request);
        try {
            $response = $slashHandlerRegistry->handle($slackRequest);
        } catch (\Exception $e) {
            $response = new SlackEphemeralResponse('Crap, something broke down: '.$e->getMessage());
        }

        if (empty($response)) {
            $response = new SlackEphemeralResponse('I have no idea what I should do with this...');
        }

        return $response;
    }
}
