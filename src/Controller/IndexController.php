<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController
{
    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        return new Response('Anna at your service');
    }
}
