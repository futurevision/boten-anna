<?php

namespace App\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class EmptySlackResponse extends SlackResponse
{
    public function __construct()
    {
        $this->headers = new ResponseHeaderBag([]);
        $this->setContent('');
        $this->setStatusCode(self::HTTP_OK);
    }

    public function send(): JsonResponse
    {
        $this->sendHeaders();

        return $this;
    }
}
