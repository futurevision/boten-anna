<?php

namespace App\Response;

class SlackInChannelResponse extends SlackResponse
{
    /**
     * @param string $text
     */
    public function __construct(string $text)
    {
        parent::__construct($text, SlackResponse::RESPONSE_TYPE_IN_CHANNEL);
    }
}
