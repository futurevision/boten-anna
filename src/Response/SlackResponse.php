<?php

namespace App\Response;

use App\Response\SlackResponse\SlackResponseAttachment;
use Symfony\Component\HttpFoundation\JsonResponse;

class SlackResponse extends JsonResponse
{
    const RESPONSE_TYPE_IN_CHANNEL = 'in_channel';
    const RESPONSE_TYPE_EPHEMERAL = 'ephemeral';

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $responseType;

    /**
     * @var SlackResponseAttachment[]
     */
    private $attachments = [];

    /**
     * @var bool
     */
    private $markupProcessing = true;

    /**
     * @param string $text
     * @param string $responseType
     */
    public function __construct(string $text, string $responseType)
    {
        $this->setText($text);
        $this->setResponseType($responseType);

        parent::__construct($this->toArray());
    }

    /**
     * @param string $text
     * @return SlackResponse
     */
    public function setText(string $text): SlackResponse
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param string $responseType
     * @return SlackResponse
     */
    public function setResponseType(string $responseType): SlackResponse
    {
        if (!in_array($responseType, [self::RESPONSE_TYPE_IN_CHANNEL, self::RESPONSE_TYPE_EPHEMERAL])) {
            throw new \InvalidArgumentException('Unsupported response type');
        }

        $this->responseType = $responseType;

        return $this;
    }

    /**
     * @param SlackResponseAttachment $attachment
     * @return SlackResponse
     */
    public function addAttachment(SlackResponseAttachment $attachment): SlackResponse
    {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Disable markdown processing
     * https://api.slack.com/docs/message-formatting#disabling_markup_processing
     *
     * @return SlackResponse
     */
    public function disableMarkupProcessing(): SlackResponse
    {
        $this->markupProcessing = false;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * {@inheritdoc}
     */
    public function send(): JsonResponse
    {
        $this->setData($this->toArray());

        return parent::send();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'response_type' => $this->responseType,
        ];
        if (!empty($this->text)) {
            $data['text'] = $this->text;
        }
        if ($this->markupProcessing === false) {
            $data['mrkdwn'] = $this->markupProcessing;
        }

        // @todo handle attachments

        return $data;
    }
}
