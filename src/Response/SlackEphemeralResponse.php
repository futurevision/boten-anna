<?php

namespace App\Response;

class SlackEphemeralResponse extends SlackResponse
{
    /**
     * @param string $text
     */
    public function __construct(string $text)
    {
        parent::__construct($text, SlackResponse::RESPONSE_TYPE_EPHEMERAL);
    }
}
