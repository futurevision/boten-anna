<?php

namespace App\Response\SlackResponse\SlackResponseAttachment;

class Author
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $link;

    /**
     * @var string|null
     */
    private $icon;

    /**
     * @param string $name
     * @param string $link
     * @param string $icon
     */
    public function __construct(string $name, string $link = null, string $icon = null)
    {
        $this->name = $name;
        $this->link = $link;
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'author_name' => $this->name
        ];
        if (!empty($this->link)) {
            $data['author_link'] = $this->link;
        }
        if (!empty($this->icon)) {
            $data['author_icon'] = $this->icon;
        }

        return $data;
    }
}
