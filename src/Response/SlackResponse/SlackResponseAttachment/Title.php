<?php

namespace App\Response\SlackResponse\SlackResponseAttachment;

class Title
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $link;

    /**
     * @param string $title
     * @param string $link
     */
    public function __construct(string $title, string $link = null)
    {
        $this->title = $title;
        $this->link = $link;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'title ' => $this->title
        ];
        if (!empty($this->link)) {
            $data['title _link'] = $this->link;
        }

        return $data;
    }
}
