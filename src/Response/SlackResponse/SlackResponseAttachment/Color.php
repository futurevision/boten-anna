<?php

namespace App\Response\SlackResponse\SlackResponseAttachment;

class Color
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $color
     */
    public function __construct(string $color)
    {
        if (in_array($color, ['good', 'warning', 'danger'])) {
            $this->value = $color;
        }
        elseif (preg_match('~^(#?)[a-fA-F0-9]{6}$~', $color)) {
            $this->value = '#'.ltrim(strtolower($color), '#');
        }
        else {
            throw new \InvalidArgumentException('Color can be "good", "warning", "danger" or a hex color');
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'color' => $this->value
        ];
    }
}
