<?php

namespace App\Response\SlackResponse;

use App\Response\SlackResponse\SlackResponseAttachment\Author;
use App\Response\SlackResponse\SlackResponseAttachment\Color;
use App\Response\SlackResponse\SlackResponseAttachment\Title;

class SlackResponseAttachment
{
    /**
     * @var string|null
     */
    private $fallback;

    /**
     * @var Color|null
     */
    private $color;

    /**
     * @var string|null
     */
    private $pretext;

    /**
     * @var Author|null
     */
    private $author;

    /**
     * @var Title|null
     */
    private $title;

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var Image
     */
    private $image;

    /**
     * @var Thumb
     */
    private $thumb;

    /**
     * @var Footer
     */
    private $footer;

    private $timestamp;

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];
        foreach (get_object_vars($this) as $property => $value) {
            if (is_object($value)) {
                $data += $value->toArray();
            }
            if (is_scalar($value)) {
                $data[$property] = $value;
            }
        }

        return $data;
    }
}
