<?php

namespace App\Console;

use App\Request\SlackRequest\ChannelId;
use App\Response\SlackInChannelResponse;
use App\Service\Slack;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PostChannelMessage extends Command
{

    /**
     * @var Slack
     */
    private $slack;



    /**
     * @var string
     */
    protected static $defaultName = 'anna:post-channel-message';

    /**
     * @param Slack $slack
     */
    public function __construct(Slack $slack)
    {
        $this->slack = $slack;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument(
                'channel',
                InputArgument::REQUIRED,
                'Channel'
            )
            ->addArgument(
                'text',
                InputArgument::REQUIRED,
                'Text'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->slack->postInChannel(
            new ChannelId($input->getArgument('channel')),
            new SlackInChannelResponse($input->getArgument('text'))
        );
    }
}
