<?php

namespace App\Tests\Response\SlackResponse\SlackResponseAttachment;

use App\Response\SlackResponse\SlackResponseAttachment\Color;
use PHPUnit\Framework\TestCase;

class ColorTest extends TestCase
{
    /**
     * @dataProvider validColors
     */
    public function testCreateValid($value, $expected)
    {
        $color = new Color($value);
        $this->assertEquals($expected, $color->toArray()['color']);
    }

    /**
     * @dataProvider invalidColors
     */
    public function testCreateInvalid($value)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Color($value);
    }

    /**
     * @return array
     */
    public function validColors(): array
    {
        return [
            ['good', 'good'],
            ['warning', 'warning'],
            ['danger', 'danger'],
            ['#cccccc', '#cccccc'],
            ['#FFFFFF', '#ffffff'],
            ['ffffff', '#ffffff'],
            ['abcdef', '#abcdef'],
            ['000000', '#000000'],
        ];
    }

    /**
     * @return array
     */
    public function invalidColors(): array
    {
        return [
            ['test'],
            ['fine'],
            ['ok'],
            ['abcdefgh'],
            ['zzzzzz']
        ];
    }
}
