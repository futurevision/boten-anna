<?php

namespace App\Tests\Controller;

use App\Response\SlackEphemeralResponse;
use App\Response\SlackInChannelResponse;
use App\Request\SlackRequest\UserId;
use App\Service\Biertijd;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SlashCommandControllerTest extends WebTestCase
{
    public function testNoCommand()
    {
        $client = static::createClient();

        $client->request('POST', '/slash-command-handler');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertInstanceOf(SlackEphemeralResponse::class, $client->getResponse());

        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('ephemeral', $data['response_type']);
        $this->assertEquals('I have no idea what I should do with this...', $data['text']);
    }

    public function testBeerCommand()
    {
        $client = static::createClient();

        $biertijdResponse = new Biertijd\BiertijdStatus([
            'status' => true
        ], new \DateTimeZone('Europe/Amsterdam'));

        $biertijd = \Mockery::mock(Biertijd::class);
        $biertijd->expects('getStatus')->andReturn($biertijdResponse);

        static::$container->set('test.'.Biertijd::class, $biertijd);

        $client->request('POST', '/slash-command-handler', [
            'command' => '/beer'
        ]);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertInstanceOf(SlackInChannelResponse::class, $client->getResponse());

        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('in_channel', $data['response_type']);
        $this->assertEquals('YESSSSSSS!!!!11111111', $data['text']);
    }

    public function testBeerFromRichardCommand()
    {
        $client = static::createClient();

        $client->request('POST', '/slash-command-handler', [
            'command' => '/beer',
            'user_id' => UserId::USER_ID_RICHARD_NIEUWENHUIS
        ]);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertInstanceOf(SlackInChannelResponse::class, $client->getResponse());

        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('in_channel', $data['response_type']);
        $this->assertEquals('Who is Richard?', $data['text']);
    }
}
