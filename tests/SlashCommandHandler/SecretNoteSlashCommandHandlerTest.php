<?php

namespace App\Tests\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Service\Biertijd\BiertijdStatus;
use App\Service\SecretNote;
use App\Service\Slack;
use App\SlashCommandHandler\BiertijdSlashCommandHandler;
use App\SlashCommandHandler\SecretNoteCommandHandler;
use GuzzleHttp\Client;
use Http\Client\Common\HttpMethodsClient;
use PHPUnit\Framework\TestCase;
use App\Service\Biertijd;
use Symfony\Component\HttpFoundation\Request;

class SecretNoteSlashCommandHandlerTest extends TestCase
{

    public function testSecretNoteHandleNotInChannel()
    {
        $slackRequest = new SlackRequest(
            new Request([], ['command' => '/secretnote', 'text' => 'my note'])
        );

        $secretNote = \Mockery::mock(SecretNote::class);
        $slack = \Mockery::mock(Slack::class);

        $handler = new SecretNoteCommandHandler($secretNote, $slack);
        $slackResponse = $handler->handle($slackRequest);

        $data = json_decode($slackResponse->getContent(), true);
        $this->assertEquals(
            'You can only use this feature in direct messages',
            $data['text']
        );
    }

    public function testSecretNoteHandle()
    {
        $slackRequest = new SlackRequest(
            new Request(
                [],
                [
                    'command' => '/secretnote',
                    'text' => 'my note',
                    'channel_name' => 'directmessage',
                    'channel_id' => 'ABCDEFGH'
                ]
            )
        );

        $secretNote = \Mockery::mock(SecretNote::class);
        $secretNote->expects('createNote')->once()->andReturn(new SecretNote\SecretNoteResponse([
            'url' => 'https://mediamonks.com',
            'expires_at' => new \DateTimeImmutable(),
            'expires_after' => '5 minutes'
        ]));

        $slack = \Mockery::mock(Slack::class);
        $slack->expects('chatPostMessage')->once();

        $handler = new SecretNoteCommandHandler($secretNote, $slack);
        $slackResponse = $handler->handle($slackRequest);

        $data = json_decode($slackResponse->getContent(), true);
        $this->assertEquals(
            'Your secret note was sent!',
            $data['text']
        );
    }
}
