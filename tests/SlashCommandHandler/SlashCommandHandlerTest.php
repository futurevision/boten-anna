<?php

namespace App\Tests\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Service\Biertijd\BiertijdStatus;
use App\Service\SecretNote;
use App\Service\Slack;
use App\SlashCommandHandler\BiertijdSlashCommandHandler;
use App\SlashCommandHandler\SecretNoteCommandHandler;
use GuzzleHttp\Client;
use Http\Client\Common\HttpMethodsClient;
use PHPUnit\Framework\TestCase;
use App\Service\Biertijd;
use Symfony\Component\HttpFoundation\Request;

class SlashCommandHandlerTest extends TestCase
{
    /**
     * @dataProvider getSupportsRequests
     *
     * @param SlackRequest $request
     * @param $supports
     */
    public function testBeertimeSupports(SlackRequest $request, $supports)
    {
        $biertijd = \Mockery::mock(Biertijd::class);
        $handler = new BiertijdSlashCommandHandler($biertijd);

        $this->assertEquals($supports, $handler->supports($request));
    }

    /**
     * @return \Generator
     */
    public function getSupportsRequests()
    {
        yield [new SlackRequest(new Request()), false];
        yield [new SlackRequest(new Request([], ['command' => '/baer'])), false];
        yield [new SlackRequest(new Request([], ['command' => '/bier'])), true];
        yield [new SlackRequest(new Request([], ['command' => '/beer'])), true];
        yield [new SlackRequest(new Request([], ['command' => 'bier'])), true];
        yield [new SlackRequest(new Request([], ['command' => 'beer'])), true];
        yield [new SlackRequest(new Request([], ['command' => 'beerio'])), true];
    }

    /**
     * @dataProvider getHandleRequests
     *
     * @param string $expected
     * @param boolean $isBeerTime
     * @param string $next
     *
     * @throws \Http\Client\Exception
     */
    public function testBeertimeHandle(string $expected, bool $isBeerTime, string $next = null)
    {
        $biertijdResponse = \Mockery::mock(BiertijdStatus::class);
        $biertijdResponse->expects('getStatus')->once()->andReturn($isBeerTime);
        if (!$isBeerTime) {
            $dateTime = new \DateTime;
            $dateTime->modify($next);
            $biertijdResponse->expects('getNext')->once()->andReturn(\DateTimeImmutable::createFromMutable($dateTime));
        }

        $biertijd = \Mockery::mock(Biertijd::class);
        $biertijd->expects('getStatus')->andReturn($biertijdResponse);

        $slackRequest = new SlackRequest(new Request([], ['command' => '/bier', 'user_id' => 'ABCDEF']));

        $handler = new BiertijdSlashCommandHandler($biertijd);
        $slackResponse = $handler->handle($slackRequest);

        $data = json_decode($slackResponse->getContent(), true);

        if ($isBeerTime) {
            $this->assertContains($expected, $data['text']);
        }
        else {
            $this->assertEquals(sprintf('Nope, next beer time is in %s', $expected), $data['text']);
        }
    }

    public function getHandleRequests()
    {
        yield ['YES', true];
        yield ['5 days, 0 hours, 0 minutes', false, '+5 days'];
        yield ['5 days, 1 hour, 0 minutes', false, '+5 days, 1 hour'];
        yield ['5 days, 1 hour, 1 minute', false, '+5 days, 1 hour, 1 minute'];
        yield ['1 hour, 1 minute', false, '+1 hour, 1 minute'];
        yield ['1 hour, 2 minutes', false, '+1 hour, 2 minutes'];
        yield ['1 minute', false, '+1 minute'];
        yield ['1 minute, 1 second', false, '+1 minute, 1 second'];
        yield ['7 minutes, 15 seconds', false, '+7 minute, 15 seconds'];
        yield ['3 days, 5 hours, 2 minutes', false, '+3 days, 5 hours, 2 minutes, 1 second'];
        yield ['3 days, 1 hour, 2 minutes', false, '+3 days, 1 hour, 2 minutes, 1 second'];
    }
}
