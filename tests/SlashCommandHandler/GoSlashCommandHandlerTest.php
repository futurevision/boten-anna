<?php

namespace App\Tests\SlashCommandHandler;

use App\Request\SlackRequest;
use App\Service\MediaMonksGo;
use App\SlashCommandHandler\GoSlashCommandHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class GoSlashCommandHandlerTest extends TestCase
{

    public function testGoWithoutArguments()
    {
        $slackRequest = new SlackRequest(
            new Request([], ['command' => '/go', 'text' => 'beer'])
        );

        $go = \Mockery::mock(MediaMonksGo::class);
        $go->expects('getUrl')->once()->andReturn('https://ishetalbiertijd.nl');

        $handler = new GoSlashCommandHandler($go);
        $slackResponse = $handler->handle($slackRequest);

        $data = json_decode($slackResponse->getContent(), true);
        $this->assertContains(
            'https://ishetalbiertijd.nl',
            $data['text']
        );
    }
}
