<?php

namespace App\Tests\Request;

use App\Request\SlackRequest;
use App\Request\SlackRequest\ChannelId;
use App\Request\SlackRequest\ChannelName;
use App\Request\SlackRequest\Command;
use App\Request\SlackRequest\ResponseUrl;
use App\Request\SlackRequest\TeamDomain;
use App\Request\SlackRequest\TeamId;
use App\Request\SlackRequest\Text;
use App\Request\SlackRequest\TriggerId;
use App\Request\SlackRequest\UserId;
use App\Request\SlackRequest\UserName;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class SlackRequestTest extends TestCase
{
    public function testGetVOs()
    {
        $request = new Request();
        $slackRequest = new SlackRequest($request);
        $this->assertInstanceOf(ChannelId::class, $slackRequest->getChannelId());
        $this->assertInstanceOf(ChannelName::class, $slackRequest->getChannelName());
        $this->assertInstanceOf(Command::class, $slackRequest->getCommand());
        $this->assertInstanceOf(\DateTimeImmutable::class, $slackRequest->getCreated());
        $this->assertInstanceOf(ResponseUrl::class, $slackRequest->getResponseUrl());
        $this->assertInstanceOf(TeamDomain::class, $slackRequest->getTeamDomain());
        $this->assertInstanceOf(TeamId::class, $slackRequest->getTeamId());
        $this->assertInstanceOf(Text::class, $slackRequest->getText());
        $this->assertInstanceOf(TriggerId::class, $slackRequest->getTriggerId());
        $this->assertInstanceOf(UserId::class, $slackRequest->getUserId());
        $this->assertInstanceOf(UserName::class, $slackRequest->getUserName());
    }
}
