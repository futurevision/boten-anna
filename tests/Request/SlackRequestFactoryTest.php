<?php

namespace App\Tests\Request;

use App\Request\SlackRequest;
use App\Request\SlackRequestFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SlackRequestFactoryTest extends TestCase
{
    const TIME = 1;
    const SIGNATURE = 'v0=89d35d45bcecedbc907b01065db0a7e260cfe599b80aead4c8098efb4f4ffb54';

    public function testFromRequestWithValidSignature()
    {
        $slackRequestFactory = new SlackRequestFactory('secret_key_from_slack');
        $request = new Request();
        $request->headers->set(SlackRequest::REQUEST_TIME_HEADER, self::TIME);
        $request->headers->set(SlackRequest::REQUEST_SIGNATURE, self::SIGNATURE);
        $this->assertInstanceOf(SlackRequest::class, $slackRequestFactory->fromRequest($request));
    }

    public function testFromRequestWithInvalidSignature()
    {
        $this->expectException(AccessDeniedHttpException::class);

        $slackRequestFactory = new SlackRequestFactory('wrong_key');
        $request = new Request();
        $request->headers->set(SlackRequest::REQUEST_TIME_HEADER, self::TIME);
        $request->headers->set(SlackRequest::REQUEST_SIGNATURE, self::SIGNATURE);
        $slackRequestFactory->fromRequest($request);
    }

    public function testFromRequestWithInvalidSignatureWithoutVerification()
    {
        $slackRequestFactory = new SlackRequestFactory('wrong_key', false);
        $request = new Request();
        $request->headers->set(SlackRequest::REQUEST_TIME_HEADER, self::TIME);
        $request->headers->set(SlackRequest::REQUEST_SIGNATURE, self::SIGNATURE);
        $this->assertInstanceOf(SlackRequest::class, $slackRequestFactory->fromRequest($request));
    }
}
