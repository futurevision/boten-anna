<?php

namespace App\Tests\Request\SlackRequest;

use App\Request\SlackRequest\Command;
use PHPUnit\Framework\TestCase;

class AbstractTextTest extends TestCase
{
    public function testGetValue()
    {
        $command = new Command('test');
        $this->assertEquals('test', $command->getValue());
        $this->assertFalse($command->isEmpty());
    }

    public function testStartsWith()
    {
        $command = new Command('test');
        $this->assertTrue($command->startsWith('tes'));
        $this->assertTrue($command->startsWith('test'));
        $this->assertFalse($command->startsWith('xtest'));
    }

    public function testIs()
    {
        $command = new Command('test');
        $this->assertTrue($command->is('test'));
        $this->assertFalse($command->is('xtest'));
    }

    public function testContains()
    {
        $command = new Command('test');
        $this->assertTrue($command->contains('tes'));
        $this->assertTrue($command->contains('test'));
        $this->assertFalse($command->contains('xtest'));
    }

    public function testMatches()
    {
        $command = new Command('testing');
        $this->assertTrue($command->matches('~^(test|tes)~'));
        $this->assertTrue($command->matches('~(ing)$~'));
        $this->assertFalse($command->matches('~^(media|monks)~'));
    }
}
